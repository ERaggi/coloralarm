#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    mView = new QGraphicsView();
    mScene = new QGraphicsScene();
    ui->graphicsView->setScene(mScene);

    QFont font;
    font.setPixelSize(10);
    font.setBold(false);
    font.setFamily("Calibri");

    mText = new QGraphicsTextItem;
    mText->setPos(150,70);
    mScene->addText(tr("Boat outside alarm area"))->setDefaultTextColor(Qt::black);

    model = new QStringListModel();
    ui->listView->setModel(model);
    ui->listView->setEditTriggers(QAbstractItemView::NoEditTriggers);

    emptyIndex();

    connect(ui->listView, SIGNAL(loggingUpdated()), this, SLOT(updateListView(const QString &)));
    connect(ui->graphicsView, SIGNAL(clicked(const QModelIndex &)), this, SLOT(on_listView_activated(const QModelIndex &index)));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::changeColorDetection()
{
}

void MainWindow::updateListView(const QString & message)
{
    if(model->insertRow(model->rowCount())) {
        QModelIndex index = model->index(model->rowCount() - 1, 0);
        model->setData(index, message);
        ui->listView->scrollTo(index);
    }
}

void MainWindow::emptyIndex()
{
    QModelIndex index;
    ui->listView->setCurrentIndex(index);
    if(index.data() == "")
    {
        return;
    }

}

void MainWindow::on_pushButton_clicked()
{
    QString str = ui->lineEdit->text();
    model->insertRow(model->rowCount());
    QModelIndex index = model->index(model->rowCount()-1);
    model->setData(index, str);
    ui->listView->scrollToBottom();
}

void MainWindow::on_checkBoxRedArea_toggled(bool checked)
{
    if(checked)
    {
        ui->checkBoxYellowArea->setChecked(false);
        ui->checkBoxGreenArea->setChecked(false);
    }
}

void MainWindow::on_checkBoxYellowArea_toggled(bool checked)
{
    if(checked)
    {
        ui->checkBoxRedArea->setChecked(false);
        ui->checkBoxGreenArea->setChecked(false);
    }
}

void MainWindow::on_checkBoxGreenArea_toggled(bool checked)
{
    if(checked)
    {
        ui->checkBoxRedArea->setChecked(false);
        ui->checkBoxYellowArea->setChecked(false);
    }
}

void MainWindow::on_comboBox_currentIndexChanged(const QString &arg1)
{
    QString list = ui->comboBox->currentText();
    ui->lineEdit->setText(list);
    Q_UNUSED(arg1)
}

void MainWindow::on_listView_entered(const QModelIndex &index)
{
}

void MainWindow::on_listView_activated(const QModelIndex &index)
{
    QStringList allStrings = model->stringList();
    QString last = allStrings.last();
    // if [ INFO] Minimum Distance: 5 inside QListView
    // Than change color of the QGraphicsView background to red
    if(last.startsWith("[ INFO] Minimum Distance: 5"))
    {
        ui->graphicsView->setBackgroundBrush(QColor(Qt::red));
    }
    // if [ INFO] Minimum Distance: 15 inside QListView
    // Than change color of the QGraphicsView background to yellow
    else if(last.startsWith("[ INFO] Minimum Distance: 10"))
    {
        ui->graphicsView->setBackgroundBrush(QColor(Qt::yellow));
    }
    // if [ INFO] Minimum Distance: 15 inside QListView
    // Than change color of the QGraphicsView background to green
    else if(last.startsWith("[ INFO] Minimum Distance: 15"))
    {
        ui->graphicsView->setBackgroundBrush(QColor(Qt::green));
    }
    Q_UNUSED(index)
}
