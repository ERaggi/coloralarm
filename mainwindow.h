#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QGraphicsView>
#include <QGraphicsScene>
#include <QGraphicsTextItem>
#include <QStringListModel>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE



class StringList : public QStringListModel
{
public:
  void append (const QString& string){
    insertRows(rowCount(), 1);
    setData(index(rowCount()-1), string);
  }
  StringList& operator<<(const QString& string){
    append(string);
    return *this;
  }
};

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    void changeColorDetection();
    void updateListView();
    void updateListView(const QString & message);
    void emptyIndex();


signals:
    void loggingUpdated();

private slots:
    void on_pushButton_clicked();
    void on_checkBoxRedArea_toggled(bool checked);
    void on_checkBoxYellowArea_toggled(bool checked);
    void on_checkBoxGreenArea_toggled(bool checked);
    void on_comboBox_currentIndexChanged(const QString &arg1);

    void on_listView_entered(const QModelIndex &index);
    void on_listView_activated(const QModelIndex &index);

private:
    Ui::MainWindow *ui;
    QGraphicsView *mView;
    QGraphicsScene *mScene;
    QGraphicsTextItem *mText;
    StringList *newString;
    QStringListModel *model;

};
#endif // MAINWINDOW_H
